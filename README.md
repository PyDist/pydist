# PyDist 1.0 

Program, który powstał w ramach pracy magisterskiej.

Służy on do graficznej wizualizacji napręźeń powstałych w makromolekułach. 

Nazwa oprogramowania nawiązuje zarówno do języka w jakim zostało napisane - Python jak i głównego rdzenia programu jakim jest PyMOL, natomiast człon "dist" pochodzi od angielskiego distance - odległość, w tym wypadku pomiędzy atomami.
